package br.com.senac.salesys;

import android.icu.text.UFormat;

import java.io.Serializable;

/**
 * Created by sala302b on 31/01/2018.
 */

public class Cliente implements Serializable{

    private String nomeCompleto;
    private String cidade;
    private String uf;
    private String profissao;
    private String nomeEmpresa;
    private String telefone;
    private String email;
    private String observacoes;

    public Cliente(){

    }

    public Cliente(String nomeCompleto, String cidade, String uf, String profissao, String nomeEmpresa, String telefone, String email, String observacoes){
        this.nomeCompleto = nomeCompleto;
        this.cidade = cidade;
        this.uf = uf;
        this.profissao = profissao;
        this.nomeEmpresa = nomeEmpresa;
        this.telefone = telefone;
        this.email = email;
        this.observacoes = observacoes;

    }

    @Override
    public String toString() {
        return ("Nome: " + nomeCompleto + "\nCidade: " + cidade + "\nUF: " + uf + "\nProfissão: " + profissao + "\nNome da Empresa: " + nomeEmpresa + "\nTelefone: " + telefone + "\nEmail: " + email + "\nObservações: " + observacoes);
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getProfissao() {
        return profissao;
    }

    public void setProfissao(String profissao) {
        this.profissao = profissao;
    }

    public String getNomeEmpresa() {
        return nomeEmpresa;
    }

    public void setNomeEmpresa(String nomeEmpresa) {
        this.nomeEmpresa = nomeEmpresa;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes(String observacoes) {
        this.observacoes = observacoes;
    }
}
