package br.com.senac.salesys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ClienteActivity extends AppCompatActivity {

    private List<String> listaEstados = new ArrayList<>();

    static String CLIENTE;

    private EditText txtNomeCompleto;
    private EditText txtCidade;
    private Spinner spnEstados;
    private EditText txtProfissao;
    private EditText txtNomeEmpresa;
    private EditText txtTelefone;
    private EditText txtEmail;
    private EditText txtObservacoes;
    private Button btnSalvar;
    private Button btnVoltar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente);

        listaEstados.add("AC");
        listaEstados.add("AL");
        listaEstados.add("AM");
        listaEstados.add("AP");
        listaEstados.add("BA");
        listaEstados.add("CE");
        listaEstados.add("DF");
        listaEstados.add("ES");
        listaEstados.add("GO");
        listaEstados.add("MA");
        listaEstados.add("MG");
        listaEstados.add("MS");
        listaEstados.add("MT");
        listaEstados.add("PA");
        listaEstados.add("PB");
        listaEstados.add("PE");
        listaEstados.add("PI");
        listaEstados.add("PR");
        listaEstados.add("RJ");
        listaEstados.add("RN");
        listaEstados.add("RO");
        listaEstados.add("RR");
        listaEstados.add("RS");
        listaEstados.add("SC");
        listaEstados.add("SE");
        listaEstados.add("SP");
        listaEstados.add("TO");


        txtNomeCompleto = findViewById(R.id.txtNomeCompleto);
        txtCidade = findViewById(R.id.txtCidade);
        spnEstados = findViewById(R.id.spnEstados);
        txtProfissao = findViewById(R.id.txtProfissao);
        txtNomeEmpresa = findViewById(R.id.txtNomeEmpresa);
        txtTelefone = findViewById(R.id.txtTelefone);
        txtEmail = findViewById(R.id.txtEmail);
        txtObservacoes = findViewById(R.id.txtObservacao);
        btnSalvar = findViewById(R.id.btnSalvar);
        btnVoltar = findViewById(R.id.btnVoltar);


        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, listaEstados);
        spnEstados.setAdapter(adapter);


        btnSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String nome = txtNomeCompleto.getText().toString();
                String cidade = txtCidade.getText().toString();
                String uf = (String)spnEstados.getSelectedItem().toString();
                String profissao = txtProfissao.getText().toString();
                String nomeEmpresa = txtNomeEmpresa.getText().toString();
                String telefone = txtTelefone.getText().toString();
                String email = txtEmail.getText().toString();
                String observacoes = txtObservacoes.getText().toString();

                Cliente cliente = new Cliente(nome, cidade, uf, profissao, nomeEmpresa, telefone, email, observacoes);

                System.out.println(cliente);

                Intent intent = new Intent();
                intent.putExtra(CLIENTE, cliente);
                setResult(RESULT_OK, intent); //seta um resultado antes de voltar para a activity anterior

                finish(); //volta para a activity anterior



            }
        });


    }
}
