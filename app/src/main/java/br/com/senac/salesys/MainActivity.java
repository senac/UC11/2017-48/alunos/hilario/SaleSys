package br.com.senac.salesys;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Cliente> listaCliente = new ArrayList<>();

    private static final int MAIN_TO_CLIENTE = 1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.novo:
                Log.i("@MENU", "Clicou em Novo");
                break;

            case R.id.sobre:
                Log.i("@MENU", "Clicou em Sobre");
                break;

        }
        return true;
    }

    public void novo(MenuItem item){
        Intent intent = new Intent(MainActivity.this, ClienteActivity.class);
        startActivityForResult(intent, MAIN_TO_CLIENTE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == MAIN_TO_CLIENTE) {
            if (resultCode == RESULT_OK) {

                //CONFERIR ESSA PARTE AQUIIIIIIIIIIIIIIIIIIIIIIIIII222222222222222222222222222222222222222222222222
                Cliente cliente = (Cliente)data.getSerializableExtra(ClienteActivity.CLIENTE);
                listaCliente.add(cliente);
                Toast.makeText(this, "Nome: " + listaCliente, Toast.LENGTH_SHORT).show();




            } else if (resultCode ==RESULT_CANCELED){
                Toast.makeText(this, "RESULT_CANCELED", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void sobre(MenuItem item){
        Toast versao = Toast.makeText(MainActivity.this,"Salesys\nv1.0b",Toast.LENGTH_LONG);
        versao.show();
//        Intent intent = new Intent(MainActivity.this, ClienteActivity.class); // MUDAR PARA SOBREEE
//        startActivity(intent);
    }
}
